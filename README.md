Kredym is the malagasy API for managing online payments.

Requires: Java 11, Postgres 10

To run Kredym:
* create an empty Postgres database named `kredym` and set username/password to `postgres`
* at the root folder of this project, on `kredym` branch, execute: `$ ./mvnw spring-boot:run`
* visit `http://127.0.0.1:8080/swagger-ui.html` for getting the list of exposed endpoints

See `LICENSE.md` for licensing. Then see Gitlab Issues if you want to contribute.