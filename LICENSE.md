This project wholly belongs to Fonenantsoa Maurica, `fonenantsoa.maurica@gmail.com`

Contributor names will be publicly disclosed upon request. However, code submitted by contributors will wholly belong to Fonenantsoa Maurica.