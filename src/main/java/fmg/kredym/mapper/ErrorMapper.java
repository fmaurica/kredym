package fmg.kredym.mapper;

import fmg.kredym.exception.BadRequest;
import fmg.kredym.exception.NotAuthorized;
import fmg.kredym.exception.NotFound;
import fmg.kredym.exception.NotImplemented;
import fmg.kredym.model.Id;
import fmg.kredym.model.external.Error;
import fmg.kredym.security.IdCipher;
import org.springframework.stereotype.Component;

import static fmg.kredym.model.external.Error.Type;

@Component
public class ErrorMapper {

    private final IdCipher idCipher;

    public ErrorMapper(IdCipher idCipher) {
        this.idCipher = idCipher;
    }

    public Error mapToExternal(BadRequest e) {
        return Error.of(Type.BAD_REQUEST, e.getMessage());
    }

    public Error mapToExternal(NotAuthorized e) {
        return Error.of(Type.NOT_AUTHORIZED, e.getMessage());
    }

    public Error mapToExternal(NotFound e) {
        int internalId = e.getId().getValue();
        String externalId = Id.IMPOSSIBLE_VALUE == internalId ?
                "<id you provided>" :
                idCipher.encrypt(internalId + "");
        String externalMessage = NotFound.getErrorMessage(e.getResourceType(), externalId);
        return Error.of(Type.NOT_FOUND, externalMessage);
    }

    public Error mapToExternal(Exception e) {
        return Error.of(Type.INTERNAL_ERROR, e.getMessage());
    }

    public Error mapToExternal(NotImplemented e) {
        return Error.of(Type.NOT_IMPLEMENTED, e.getMessage());
    }

}
