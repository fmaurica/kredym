package fmg.kredym.mapper;

import fmg.kredym.exception.NotImplemented;
import fmg.kredym.model.Account;
import fmg.kredym.model.Money;
import fmg.kredym.model.Topping;
import fmg.kredym.model.ToppingPrint;
import fmg.kredym.model.external.ToppingPrintTemplatable;
import fmg.kredym.repository.hrepository.model.HAccount;
import fmg.kredym.repository.hrepository.model.HTopping;
import fmg.kredym.repository.hrepository.model.HToppingPrint;
import fmg.kredym.security.IdCipher;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import static java.util.stream.Collectors.toUnmodifiableList;

@Component
public class ToppingPrintMapper {

    private final IdCipher idCipher;
    private final AccountMapper accountMapper;

    private static final DateTimeFormatter DATE_FORMATTER_UTC3 =
            DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(ZoneId.of("UTC+3"));

    public ToppingPrintMapper(IdCipher idCipher, AccountMapper accountMapper) {
        this.idCipher = idCipher;
        this.accountMapper = accountMapper;
    }

    public fmg.kredym.model.external.ToppingPrint mapToExternal(ToppingPrint print) {
        Money price = print.getPrice();
        return fmg.kredym.model.external.ToppingPrint.builder()
                .id(idCipher.encrypt(print.getId().getValue() + ""))
                .price(fmg.kredym.model.external.Money.of(price.getAmount(), price.getCurrency().getCurrencyCode()))
                .distributor(print.getDistributor())
                .creationInstant(print.getCreationInstant().toString())
                .toppings(print.getToppings().stream()
                        .map(this::mapToExternal)
                        .collect(toUnmodifiableList()))
                .build();
    }

    public fmg.kredym.model.external.ToppingPrintTemplatable mapToTemplatable(
            // takes external model as input to make sure that no internal information could be leaked
            fmg.kredym.model.external.ToppingPrint print, int sequence, int sequenceMax) {
        fmg.kredym.model.external.Money price = print.getPrice();
        if (!"MGA".equals(price.getCurrency())) {
            throw new NotImplemented();
        }
        return fmg.kredym.model.external.ToppingPrintTemplatable.builder()
                .id(print.getId())
                .distributor(print.getDistributor())
                .creationDate(DATE_FORMATTER_UTC3.format(Instant.parse(print.getCreationInstant())))
                .sequence(sequence)
                .sequenceMax(sequenceMax)
                .amountInMga(print.getPrice().getAmount())
                .toppings(print.getToppings().stream()
                        .map(this::mapToTemplatable)
                        .collect(toUnmodifiableList()))
                .build();
    }

    public List<fmg.kredym.model.external.ToppingPrint> mapToExternal(List<ToppingPrint> toppingPrints) {
        return toppingPrints.stream()
                .map(this::mapToExternal)
                .collect(toUnmodifiableList());
    }

    public HToppingPrint mapToH(ToppingPrint print) {
        ToppingPrint.Id id = print.getId();
        Money price = print.getPrice();
        HToppingPrint hPrint = HToppingPrint.builder()
                .id(id == null ? null : id.getValue())
                .amount(price.getAmount())
                .currency(price.getCurrency().getCurrencyCode())
                .distributor(print.getDistributor())
                .toppings(new ArrayList<>())
                .creationInstant(print.getCreationInstant())
                .build();
        for (Topping topping : print.getToppings()) {
            hPrint.addTopping(mapToH(topping));
        }
        return hPrint;
    }

    public ToppingPrint mapToDomain(HToppingPrint hPrint) {
        return ToppingPrint.builder()
                .id(ToppingPrint.Id.of(hPrint.getId()))
                .price(Money.of(hPrint.getAmount(), Currency.getInstance(hPrint.getCurrency())))
                .toppings(hPrint.getToppings().stream().map(this::mapToDomain).collect(toUnmodifiableList()))
                .distributor(hPrint.getDistributor())
                .creationInstant(hPrint.getCreationInstant())
                .build();
    }

    /////////////////////
    ////// Topping //////
    /////////////////////

    public fmg.kredym.model.external.Topping mapToExternal(Topping topping) {
        Money money = topping.getPrice();
        Instant activationInstant = topping.getActivationInstant();
        Account activator = topping.getActivator();
        return fmg.kredym.model.external.Topping.builder()
                .id(idCipher.encrypt(topping.getId().getValue() + ""))
                .code(topping.getCode())
                .price(fmg.kredym.model.external.Money.of(money.getAmount(), money.getCurrency().getCurrencyCode()))
                .validity(topping.getValidity().toDays())
                .printId(idCipher.encrypt(topping.getPrintId().getValue() + ""))
                .activationInstant(activationInstant == null ? null : activationInstant.toString())
                .activatorId(activator == null ? null : idCipher.encrypt(activator.getId().getValue() + ""))
                .creationInstant(topping.getCreationInstant().toString())
                .build();
    }

    private ToppingPrintTemplatable.Topping mapToTemplatable(fmg.kredym.model.external.Topping topping) {
        fmg.kredym.model.external.Money price = topping.getPrice();
        if (!"MGA".equals(price.getCurrency())) {
            throw new NotImplemented();
        }
        return ToppingPrintTemplatable.Topping.builder()
                .code(topping.getCode())
                .amountInMga(price.getAmount())
                .expirationDate(DATE_FORMATTER_UTC3.format(Instant
                        .parse(topping.getCreationInstant())
                        .plus(topping.getValidity(), ChronoUnit.DAYS)))
                .build();
    }

    public HTopping mapToH(Topping topping) {
        Topping.Id id = topping.getId();
        Money money = topping.getPrice();
        Account activator = topping.getActivator();
        return HTopping.builder()
                .id(id == null ? null : id.getValue())
                .code(topping.getCode())
                .amount(money.getAmount())
                .currency(money.getCurrency().getCurrencyCode())
                .validity(topping.getValidity().toDays())
                //.toppingPrint(null) //TODO: safe?
                .activationInstant(topping.getActivationInstant())
                .activator(activator == null ? null : accountMapper.mapToH(topping.getActivator()))
                .creationInstant(topping.getCreationInstant())
                .disabled(topping.isDisabled())
                .build();
    }

    public Topping mapToDomain(HTopping hTopping) {
        HToppingPrint print = hTopping.getToppingPrint();
        HAccount activator = hTopping.getActivator();
        return Topping.builder()
                .id(Topping.Id.of(hTopping.getId()))
                .code(hTopping.getCode())
                .price(Money.of(hTopping.getAmount(), Currency.getInstance(hTopping.getCurrency())))
                .validity(Duration.ofDays(hTopping.getValidity()))
                .printId(print == null ? null : ToppingPrint.Id.of(print.getId()))
                .activationInstant(hTopping.getActivationInstant())
                .activator(activator == null ? null : accountMapper.mapToDomain(hTopping.getActivator()))
                .creationInstant(hTopping.getCreationInstant())
                .disabled(hTopping.isDisabled())
                .build();
    }

}
