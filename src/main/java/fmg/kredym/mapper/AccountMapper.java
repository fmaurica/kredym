package fmg.kredym.mapper;

import fmg.kredym.model.Account;
import fmg.kredym.model.Money;
import fmg.kredym.repository.hrepository.model.HAccount;
import fmg.kredym.security.IdCipher;
import org.springframework.stereotype.Component;

import java.util.Currency;

@Component
public class AccountMapper {

    private final IdCipher idCipher;

    public AccountMapper(IdCipher idCipher) {
        this.idCipher = idCipher;
    }

    public fmg.kredym.model.external.Account mapToExternal(Account account) {
        Money money = account.getMoney();
        return fmg.kredym.model.external.Account.builder()
                .accountId(idCipher.encrypt(account.getId().getValue() + ""))
                .money(fmg.kredym.model.external.Money.of(money.getAmount(), money.getCurrency().getCurrencyCode()))
                .name(account.getName())
                .build();
    }

    public HAccount mapToH(Account account) {
        Money money = account.getMoney();
        return HAccount.builder()
                .id(account.getId().getValue())
                .name(account.getName())
                .amount(money.getAmount())
                .currency(money.getCurrency().getCurrencyCode())
                .disabled(account.isDisabled())
                .build();
    }

    public Account mapToDomain(HAccount hAccount) {
        return Account.builder()
                .id(Account.Id.of(hAccount.getId()))
                .name(hAccount.getName())
                .money(Money.of(hAccount.getAmount(), Currency.getInstance(hAccount.getCurrency())))
                .disabled(hAccount.isDisabled())
                .build();
    }

}
