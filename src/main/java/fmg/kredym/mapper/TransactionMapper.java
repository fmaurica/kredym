package fmg.kredym.mapper;

import fmg.kredym.model.Money;
import fmg.kredym.model.Transaction;
import fmg.kredym.repository.hrepository.model.HTransaction;
import fmg.kredym.security.IdCipher;
import org.springframework.stereotype.Component;

import java.util.Currency;

@Component
public class TransactionMapper {

    private final AccountMapper accountMapper;
    private final IdCipher idCipher;

    public TransactionMapper(AccountMapper accountMapper, IdCipher idCipher) {
        this.accountMapper = accountMapper;
        this.idCipher = idCipher;
    }

    public fmg.kredym.model.external.Transaction mapToExternal(Transaction transaction) {
        Money money = transaction.getMoney();
        return fmg.kredym.model.external.Transaction.builder()
                .transactionId(idCipher.encrypt(transaction.getId().getValue() + ""))
                .sourceAccountId(idCipher.encrypt(transaction.getSource().getId().getValue() + ""))
                .destinationAccountId(idCipher.encrypt(transaction.getDestination().getId().getValue() + ""))
                .money(fmg.kredym.model.external.Money.of(money.getAmount(), money.getCurrency().getCurrencyCode()))
                .creationInstant(transaction.getCreationInstant().toString())
                .build();
    }

    public HTransaction mapToH(Transaction transaction) {
        Money money = transaction.getMoney();
        Transaction.Id id = transaction.getId();
        return HTransaction.builder()
                .id(id == null ? null : id.getValue())
                .source(accountMapper.mapToH(transaction.getSource()))
                .destination(accountMapper.mapToH(transaction.getDestination()))
                .amount(money.getAmount())
                .currency(money.getCurrency().getCurrencyCode())
                .creationInstant(transaction.getCreationInstant())
                .build();
    }

    public Transaction mapToDomain(HTransaction hTransaction) {
        return Transaction.builder()
                .id(Transaction.Id.of(hTransaction.getId()))
                .source(accountMapper.mapToDomain(hTransaction.getSource()))
                .destination(accountMapper.mapToDomain(hTransaction.getDestination()))
                .money(Money.of(hTransaction.getAmount(), Currency.getInstance(hTransaction.getCurrency())))
                .creationInstant(hTransaction.getCreationInstant())
                .build();
    }

}
