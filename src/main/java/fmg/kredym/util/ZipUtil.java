package fmg.kredym.util;

import lombok.SneakyThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtil {

    @SneakyThrows
    // Copy-pasted from https://www.baeldung.com/java-compress-and-uncompress
    public static File zipTemp(List<File> files, String fileName) {
        File temp = File.createTempFile(fileName, ".zip");

        FileOutputStream fos = new FileOutputStream(temp);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        for (File file : files) {
            FileInputStream fis = new FileInputStream(file);
            ZipEntry zipEntry = new ZipEntry(file.getName());
            zipOut.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
        }
        zipOut.close();
        fos.close();

        return temp;
    }

}
