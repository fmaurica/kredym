package fmg.kredym.util;

import lombok.SneakyThrows;

import java.io.*;

import static java.util.stream.Collectors.joining;

public class FileUtil {

    public static String readResourceFile(String path) {
        String template;
        /* Once module is packaged in a jar, the only way to access to resources is through InputStream.
         * In particular, using java.io.File does **not** work. */
        InputStream templateInputStream = FileUtil.class.getResourceAsStream(path);
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(templateInputStream))) {
            template = bufferedReader.lines().collect(joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return template;
    }

    @SneakyThrows
    public static File createTempFile(String fileName, String fileType, String content) {
        File temp = File.createTempFile(fileName, "." + fileType);
        BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
        bw.write(content);
        bw.close();
        return temp;
    }

}
