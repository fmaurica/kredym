package fmg.kredym.controller.request;

import lombok.Value;

@Value(staticConstructor = "of")
public class TopUpAccountRequest {

    private final String accountId;
    private final String code;

}
