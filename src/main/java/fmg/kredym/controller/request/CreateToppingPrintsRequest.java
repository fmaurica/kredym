package fmg.kredym.controller.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateToppingPrintsRequest {

    private int printsNumber;
    private String distributor;

}
