package fmg.kredym.controller.request;

import fmg.kredym.conf.Doc;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateTransactionRequest {

    private String sourceAccountId;
    private String destinationAccountId;

    @Schema(description = Doc.AMOUNT)
    private int amount;

    @Schema(description = Doc.CURRENCY, example = Doc.CURRENCY_EXAMPLE)
    private String currency;

}
