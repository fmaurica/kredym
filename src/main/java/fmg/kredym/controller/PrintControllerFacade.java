package fmg.kredym.controller;

import fmg.kredym.controller.request.CreateToppingPrintsRequest;
import fmg.kredym.exception.NotImplemented;
import fmg.kredym.model.ToppingPrint;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.nio.file.Files;
import java.util.Currency;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;

@RestController
@Tag(name = "Toppings")
@Slf4j
public class PrintControllerFacade {

    private final PrintController printController;

    public PrintControllerFacade(PrintController printController) {
        this.printController = printController;
    }

    public/*checked in tests*/ static final int TOPPINGS_PER_PRINT = 32;
    public static final int TOPPING_MGA_AMOUNT = 1500;
    public static final int PRINT_MGA_AMOUNT = TOPPINGS_PER_PRINT * TOPPING_MGA_AMOUNT - 5000;
    public static final Currency MGA = Currency.getInstance("MGA");
    public static final long TOPPING_VALIDITY_IN_DAYS = 21;
    private static final String TOPPING_PRINT_DEFINITION =
            "A topping print has " + TOPPINGS_PER_PRINT + " toppings. " +
                    "A topping is worth MGA" + TOPPING_MGA_AMOUNT +
                    " and is valid for " + TOPPING_VALIDITY_IN_DAYS + " days.";

    @SneakyThrows
    @PostMapping(value = "/topping/prints/zip", produces = APPLICATION_OCTET_STREAM_VALUE)
    @ResponseStatus(value = CREATED)
    @Operation(description = "Create topping prints and download them as zip. " + TOPPING_PRINT_DEFINITION)
    public void createPrintsZip(
            @RequestBody CreateToppingPrintsRequest request, HttpServletResponse response) {
        File zip = printController.createPrintsZip(request);
        response.setStatus(CREATED.value());
        response.setHeader("Content-Disposition", "attachment; filename=" + zip.getName());
        response
                // file can be downloaded many times, TODO: for how long?
                .getOutputStream()
                //TODO: avoid full charging into memory, see Spring FileSystemResource
                .write(Files.readAllBytes(zip.toPath()));
    }

    @PostMapping(value = "/topping/prints" /*TODO: super root only*/)
    @ResponseStatus(value = CREATED)
    @Operation(description = "Create topping prints. " + TOPPING_PRINT_DEFINITION)
    public List<fmg.kredym.model.external.ToppingPrint> createPrintsJson(
            @RequestBody CreateToppingPrintsRequest request) {
        return printController.createPrintsJson(request);
    }

    @GetMapping(value = "/topping/prints", produces = {"application/json", "application/zip"})
    @Operation(description = "Find topping prints by optional criteria.")
    public List<fmg.kredym.model.external.ToppingPrint> getToppingPrints(
            @PathVariable @Parameter(schema = @Schema(implementation = String.class)) ToppingPrint.Id id) {
        throw new NotImplemented();
    }

}
