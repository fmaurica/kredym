package fmg.kredym.controller.advice;

import fmg.kredym.exception.BadRequest;
import fmg.kredym.exception.NotAuthorized;
import fmg.kredym.exception.NotFound;
import fmg.kredym.exception.NotImplemented;
import fmg.kredym.mapper.ErrorMapper;
import fmg.kredym.model.external.Error;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
@Slf4j
public class InternalToExternalErrorHandler {

    private final ErrorMapper errorMapper;

    public InternalToExternalErrorHandler(ErrorMapper errorMapper) {
        this.errorMapper = errorMapper;
    }

    @ExceptionHandler(value = {BadRequest.class})
    @ApiResponse(description = "Bad request",
            responseCode = "400",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Error.class)))
    ResponseEntity<Error> handleBadRequest(BadRequest e) {
        return new ResponseEntity<>(errorMapper.mapToExternal(e), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    ResponseEntity<Error> handleBadRequest(MissingServletRequestParameterException e) {
        return handleBadRequest(new BadRequest(e.getMessage()));
    }

    @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
    ResponseEntity<Error> handleConversionFailed(MethodArgumentTypeMismatchException e) {
        String message = e.getCause()
                .getCause()
                .getMessage();
        return handleBadRequest(new BadRequest(message));
    }

    @ExceptionHandler(value = {NotAuthorized.class})
    @ApiResponse(description = "Forbidden",
            responseCode = "403",
            content/*similar to BadRequest so make doc lighter*/ = @Content(mediaType = "", schema = @Schema))
    ResponseEntity<Error> handleNotAuthorized(NotAuthorized e) {
        /* _not_ HttpsStatus.UNAUTHORIZED because, counter-intuitively, it's just for authentication
         * https://stackoverflow.com/questions/3297048/403-forbidden-vs-401-unauthorized-http-responses */
        return new ResponseEntity<>(errorMapper.mapToExternal(e), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = {NotFound.class})
    @ApiResponse(description = "Resource not found",
            responseCode = "404",
            content/*similar to BadRequest so make doc lighter*/ = @Content(mediaType = "", schema = @Schema))
    ResponseEntity<Error> handleNotFound(NotFound e) {
        return new ResponseEntity<>(errorMapper.mapToExternal(e), HttpStatus.NOT_FOUND);
    }

    /**
     * handleDefault is the default handler for exceptions without any specific @ExceptionHandler.
     * handleDefault also handles throwables.
     * Indeed, uncaught throwables raise IllegalStateException, which would ultimately finish here.
     */
    @ExceptionHandler(value = {Exception.class})
    @ApiResponse(description = "Internal server error",
            responseCode = "500",
            content/*similar to BadRequest so make doc lighter*/ = @Content(mediaType = "", schema = @Schema))
    ResponseEntity<Error> handleDefault(Exception e) {
        log.error("Internal error", e);
        return new ResponseEntity<>(errorMapper.mapToExternal(e), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {NotImplemented.class})
    @ApiResponse(description = "Not implemented",
            responseCode = "501",
            content/*similar to BadRequest so make doc lighter*/ = @Content(mediaType = "", schema = @Schema))
    ResponseEntity<Error> handleNotImplemented(NotImplemented e) {
        log.error("Not implemented", e);
        return new ResponseEntity<>(errorMapper.mapToExternal(e), HttpStatus.NOT_IMPLEMENTED);
    }

}
