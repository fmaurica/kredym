package fmg.kredym.controller;

import fmg.kredym.controller.request.TopUpAccountRequest;
import fmg.kredym.exception.NotImplemented;
import fmg.kredym.mapper.AccountMapper;
import fmg.kredym.model.Account;
import fmg.kredym.model.Topping;
import fmg.kredym.security.IdCipher;
import fmg.kredym.service.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static fmg.kredym.model.Id.IMPOSSIBLE_VALUE;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@Tag(name = "Accounts")
public class AccountController {

    private final AccountService accountService;
    private final AccountMapper accountMapper;
    private final IdCipher idCipher;

    public AccountController(AccountService accountService, AccountMapper accountMapper, IdCipher idCipher) {
        this.accountService = accountService;
        this.accountMapper = accountMapper;
        this.idCipher = idCipher;
    }

    @PostMapping(value = "/account")
    @Operation(description = "Create account")
    @ResponseStatus(value = CREATED)
    public fmg.kredym.model.external.Account createAccount() {
        throw new NotImplemented();
    }

    @PostMapping(value = "/account/debit")
    @Operation(description = "Remove money from account.")
    public fmg.kredym.model.external.Account debitAccount(@RequestBody fmg.kredym.model.external.Money money) {
        throw new NotImplemented();
    }

    @PostMapping(value = "/account/topUp")
    public fmg.kredym.model.external.Account topUpAccount(@RequestBody TopUpAccountRequest request) {
        Account.Id accountId = Account.Id.of(decryptAsInt(request.getAccountId()));
        return accountMapper.mapToExternal(
                accountService.topUp(accountId, request.getCode()));
    }

    @GetMapping("/accounts")
    @Operation(description = "Find accounts by optional criteria.")
    public List<fmg.kredym.model.external.Account> getAccount(
            @RequestParam @Parameter(schema = @Schema(implementation = String.class)) Account.Id id) {
        return accountService
                .findById(id)
                .map(accountMapper::mapToExternal)
                .map(List::of)
                .orElse(List.of());
    }

    private int decryptAsInt(String s) {
        try {
            return Integer.parseInt(idCipher.decrypt(s));
        } catch (Exception e) {
            return IMPOSSIBLE_VALUE;
        }
    }

}
