package fmg.kredym.controller;

import fmg.kredym.controller.request.CreateToppingPrintsRequest;
import fmg.kredym.mapper.ToppingPrintMapper;
import fmg.kredym.model.Money;
import fmg.kredym.model.external.ToppingPrint;
import fmg.kredym.model.external.ToppingPrintTemplatable;
import fmg.kredym.service.ToppingPrintService;
import fmg.kredym.template.TemplateEngine;
import fmg.kredym.template.ThTemplateEngine;
import fmg.kredym.util.ZipUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static fmg.kredym.controller.PrintControllerFacade.*;
import static java.util.stream.Collectors.toUnmodifiableList;

@Component
@Slf4j
public class PrintController {

    private final ToppingPrintService toppingPrintService;
    private final ToppingPrintMapper toppingPrintMapper;
    private final TemplateEngine templateEngine;

    public PrintController(ToppingPrintService toppingPrintService,
                           ToppingPrintMapper toppingPrintMapper) {
        this.toppingPrintService = toppingPrintService;
        this.toppingPrintMapper = toppingPrintMapper;
        this.templateEngine = new ThTemplateEngine();
    }

    private static final char FILENAME_SEPARATOR = '_';
    private static final char FILENAME_BADCHAR_REPLACER = '-';
    private static final String FILENAME_SEPARATOR_STR = FILENAME_SEPARATOR + "";

    @Transactional // crucial: rollback created prints if zip generation failed
    public File createPrintsZip(CreateToppingPrintsRequest request) {
        List<ToppingPrint> prints = createPrintsJson(request);
        // Files to zip
        List<File> toZip = new ArrayList<>();
        int printsSize = prints.size();
        for (int i = 0; i < printsSize; i++) {
            int sequence = i + 1;
            ToppingPrintTemplatable templatable = toppingPrintMapper.mapToTemplatable(
                    prints.get(i), sequence, printsSize);
            String pdfName = toAlphanumAndSeparator(
                    sequence + FILENAME_SEPARATOR_STR + templatable.getId());
            toZip.add(templateEngine.processToPdf(templatable, pdfName));
        }
        // Zip them
        fmg.kredym.model.external.ToppingPrint print0 = prints.get(0);
        String distributor = request.getDistributor();
        int distributorLength = distributor.length();
        String zipName = toAlphanumAndSeparator(String.join(
                FILENAME_SEPARATOR_STR,
                print0.getCreationInstant().substring(0, 10),
                distributor.substring(0, Math.min(distributorLength, 10)),
                print0.getId()));
        File zip = ZipUtil.zipTemp(toZip, zipName);
        log.info("Zip created: {}", zip.getAbsolutePath());
        return zip;
    }

    public List<fmg.kredym.model.external.ToppingPrint> createPrintsJson(CreateToppingPrintsRequest request) {
        List<fmg.kredym.model.external.ToppingPrint> prints = toppingPrintMapper.mapToExternal(
                toppingPrintService.createToppingPrints(
                        // print
                        request.getPrintsNumber(),
                        Money.of(PRINT_MGA_AMOUNT, MGA),
                        TOPPINGS_PER_PRINT,
                        request.getDistributor(),
                        // topping
                        Money.of(TOPPING_MGA_AMOUNT, MGA),
                        Duration.ofDays(TOPPING_VALIDITY_IN_DAYS)));
        log.info("Prints: {}", toIds(prints));
        return prints;
    }

    // Exotic file names are not allowed on all OS, especially on Windows
    private String toAlphanumAndSeparator(String badFilename) {
        StringBuilder sanitizedBuilder = new StringBuilder();
        char[] chars = badFilename.toCharArray();
        for (char c : chars) {
            sanitizedBuilder.append(
                    'A' <= c && c <= 'Z' || 'a' <= c && c <= 'z' || '0' <= c && c <= '9' || c == FILENAME_SEPARATOR ?
                            c :
                            FILENAME_BADCHAR_REPLACER);
        }
        return sanitizedBuilder.toString();
    }

    private List<String> toIds(List<fmg.kredym.model.external.ToppingPrint> prints) {
        return prints.stream().map(fmg.kredym.model.external.ToppingPrint::getId).collect(toUnmodifiableList());
    }

}
