package fmg.kredym.controller.converter;

import fmg.kredym.conf.Doc;
import fmg.kredym.exception.BadRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.PageRequest;

public class PaginationConverter implements Converter<String, PageRequest> {

    @Override
    public PageRequest convert(String paginationAsString) {
        int page, size;
        String badRequestMessage = "Pagination format must be " + Doc.PAGINATION;
        try {
            String[] split = paginationAsString.split(",");
            page = Integer.parseInt(split[0]);
            size = Integer.parseInt(split[1]);
        } catch (Exception e) {
            throw new BadRequest(badRequestMessage);
        }
        if (page < 0 || size > Doc.PAGINATION_MAXSIZE) {
            throw new BadRequest(badRequestMessage);
        }
        return PageRequest.of(page, size);
    }

}
