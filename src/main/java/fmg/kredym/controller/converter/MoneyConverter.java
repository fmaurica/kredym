package fmg.kredym.controller.converter;

import fmg.kredym.model.Money;
import org.springframework.core.convert.converter.Converter;

import java.util.Currency;

public class MoneyConverter implements Converter<String, Money> {

    @Override
    public Money convert(String moneyAsString) {
        String[] split = moneyAsString.split(",");
        return Money.of(Integer.parseInt(split[0]), Currency.getInstance(split[1]));
    }

}
