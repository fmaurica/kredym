package fmg.kredym.controller.converter;

import fmg.kredym.model.Transaction;
import fmg.kredym.security.IdCipher;

public class TransactionIdConverter extends IdConverter<Transaction.Id> {

    public TransactionIdConverter(IdCipher idCipher) {
        super(idCipher);
    }

    protected Transaction.Id fromInt(int id) {
        return Transaction.Id.of(id);
    }

    @Override
    protected fmg.kredym.exception.NotFound.ResourceType getResourceType() {
        return fmg.kredym.exception.NotFound.ResourceType.TRANSACTION;
    }

}
