package fmg.kredym.controller.converter;

import org.springframework.core.convert.converter.Converter;

import java.time.Instant;

public class InstantConverter implements Converter<String, Instant> {

    @Override
    public Instant convert(String s) {
        return Instant.parse(s);
    }

}
