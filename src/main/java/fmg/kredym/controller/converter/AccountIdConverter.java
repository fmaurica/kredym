package fmg.kredym.controller.converter;

import fmg.kredym.model.Account;
import fmg.kredym.security.IdCipher;
import org.springframework.stereotype.Component;

@Component
public class AccountIdConverter extends IdConverter<Account.Id> {

    public AccountIdConverter(IdCipher idCipher) {
        super(idCipher);
    }

    @Override
    protected Account.Id fromInt(int id) {
        return Account.Id.of(id);
    }

    @Override
    protected fmg.kredym.exception.NotFound.ResourceType getResourceType() {
        return fmg.kredym.exception.NotFound.ResourceType.ACCOUNT;
    }

}
