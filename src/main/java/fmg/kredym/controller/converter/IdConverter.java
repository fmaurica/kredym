package fmg.kredym.controller.converter;

import fmg.kredym.model.Id;
import fmg.kredym.security.CipherException;
import fmg.kredym.security.IdCipher;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;

@Slf4j
public abstract class IdConverter<T> implements Converter<String, T> {

    private final IdCipher idCipher;

    public IdConverter(IdCipher idCipher) {
        this.idCipher = idCipher;
    }

    @Override
    public T convert(String externalId) {
        try {
            String internalId = idCipher.decrypt(externalId);
            return fromInt(Integer.parseInt(internalId));
        } catch (CipherException | NumberFormatException e) {
            return fromInt(Id.IMPOSSIBLE_VALUE);
        }
    }

    protected abstract T fromInt(int id);

    protected abstract fmg.kredym.exception.NotFound.ResourceType getResourceType();

    @Value
    @RequiredArgsConstructor(staticName = "of")
    public static class NotFound extends RuntimeException {
        private final fmg.kredym.exception.NotFound.ResourceType resourceType;
        private final String externalId;
    }

}
