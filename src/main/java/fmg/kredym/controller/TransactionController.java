package fmg.kredym.controller;

import fmg.kredym.conf.Doc;
import fmg.kredym.controller.converter.AccountIdConverter;
import fmg.kredym.controller.request.CreateTransactionRequest;
import fmg.kredym.exception.BadRequest;
import fmg.kredym.mapper.TransactionMapper;
import fmg.kredym.model.Money;
import fmg.kredym.model.Transaction;
import fmg.kredym.service.TransactionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.Currency;
import java.util.List;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping(produces = "application/json")
@Tag(name = "Transactions", description = "money movements across accounts")
public class TransactionController {

    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final AccountIdConverter accountIdConverter;

    public TransactionController(TransactionService transactionService,
                                 TransactionMapper transactionMapper,
                                 AccountIdConverter accountIdConverter) {
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.accountIdConverter = accountIdConverter;
    }

    /* OpenAPI spec in the code?
     * Yes, if dev is also responsible for API exposure.
     * No, if there is a dedicated person for API exposure (doc writer). */
    @GetMapping("/transactions")
    @Operation(description =
            "Get list of transactions by combination of optional criteria.")
    public List<fmg.kredym.model.external.Transaction> getTransactions(
            @RequestParam(required = false) @Parameter(
                    schema = @Schema(implementation = String.class)) Transaction.Id id,
            @RequestParam(required = false) @Parameter(
                    description = Doc.MONEY,
                    example = Doc.MONEY_EXAMPLE,
                    schema = @Schema(implementation = String.class)) Money money,
            @RequestParam(required = false) @Parameter(
                    description = "Inclusive lower bound on creation date: " + Doc.INSTANT,
                    example = Doc.INSTANT_EXAMPLE,
                    schema = @Schema(implementation = String.class)) Instant from,
            @RequestParam(required = false) @Parameter(
                    description = "Inclusive upper bound on creation date: " + Doc.INSTANT,
                    example = Doc.INSTANT_EXAMPLE,
                    schema = @Schema(implementation = String.class)) Instant to,
            @RequestParam(required = false) @Parameter(
                    description = Doc.PAGINATION,
                    example = Doc.PAGINATION_EXAMPLE,
                    schema = @Schema(implementation = String.class)) PageRequest pagination) {
        if (id == null && money == null && from == null && to == null) {
            throw new BadRequest("One criteria must be provided at least");
        }
        pagination = checkPaginationForId(id, pagination);
        return transactionService
                .findByCriteria(id, money, from, to, pagination)
                .stream()
                .map(transactionMapper::mapToExternal)
                .collect(toUnmodifiableList());
    }

    private PageRequest checkPaginationForId(Transaction.Id id, PageRequest pagination) {
        if (id == null) {
            if (pagination == null) {
                throw new BadRequest("Pagination must be provided when id is not");
            }
            return pagination;
        } else {
            return PageRequest.of(0, 1); // at most 1 satisfies id
        }
    }

    @PostMapping("/transaction")
    @ResponseStatus(value = CREATED)
    @Operation(description = "Create transaction")
    public fmg.kredym.model.external.Transaction createTransaction(
            @RequestBody CreateTransactionRequest createTransactionReq) {
        Money money = Money.of(
                createTransactionReq.getAmount(),
                Currency.getInstance(createTransactionReq.getCurrency()));
        return transactionMapper.mapToExternal(
                transactionService.createTransaction(
                        accountIdConverter.convert(createTransactionReq.getSourceAccountId()),
                        accountIdConverter.convert(createTransactionReq.getDestinationAccountId()),
                        money));
    }

}
