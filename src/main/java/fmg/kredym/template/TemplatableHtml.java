package fmg.kredym.template;

public abstract class TemplatableHtml extends Templatable {

    protected String resourcesPath;

    protected abstract void setResourcesPath();

    public String getResourcesPath() {
        return resourcesPath;
    }

}
