package fmg.kredym.template;

import lombok.SneakyThrows;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Map;

public class ThTemplateEngine implements TemplateEngine {

    private final org.thymeleaf.TemplateEngine templateEngine;

    public ThTemplateEngine() {
        templateEngine = new org.thymeleaf.TemplateEngine();
    }

    @Override
    public String process(Templatable templatable) {
        Context thContext = new Context();
        thContext.setVariables(Map.of("context", templatable));
        return templateEngine.process(templatable.getTemplate(), thContext);
    }

    @SneakyThrows
    @Override
    public File processToPdf(TemplatableHtml templatableHtml, String outputName) {
        File output = File.createTempFile(outputName, ".pdf");

        templatableHtml.setResourcesPath();
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(process(templatableHtml), templatableHtml.getResourcesPath());
        renderer.layout();
        OutputStream outputStream = new FileOutputStream(output);
        renderer.createPDF(outputStream);
        outputStream.close();

        return output;
    }

}
