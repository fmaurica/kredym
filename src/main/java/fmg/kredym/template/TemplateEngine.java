package fmg.kredym.template;

import java.io.File;

public interface TemplateEngine {

    String process(Templatable templatable);

    File processToPdf(TemplatableHtml templatableHtml, String outputName);

}
