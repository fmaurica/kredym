package fmg.kredym.template;

import fmg.kredym.util.FileUtil;

public abstract class Templatable {

    protected String templatePath;

    protected abstract void setTemplatePath();

    public String getTemplate() {
        setTemplatePath();
        return FileUtil.readResourceFile(templatePath);
    }

}
