package fmg.kredym.model;

public abstract class Id {
    public static final int IMPOSSIBLE_VALUE = -1;

    public abstract int getValue();
}
