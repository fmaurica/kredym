package fmg.kredym.model;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

import java.time.Duration;
import java.time.Instant;

@Value
@RequiredArgsConstructor(staticName = "of")
@Builder(toBuilder = true)
public class Topping {

    private Id id;

    private final String code;
    private final Money price;

    private final Instant creationInstant;
    private final Duration validity;

    private final Instant activationInstant;
    private final Account activator;

    private final ToppingPrint.Id printId;

    private final boolean disabled;

    @Value
    @RequiredArgsConstructor(staticName = "of")
    @ToString
    public static class Id extends fmg.kredym.model.Id {
        private final int value;
    }

}
