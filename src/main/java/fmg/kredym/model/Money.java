package fmg.kredym.model;

import fmg.kredym.exception.NotImplemented;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.Currency;

@Value
@RequiredArgsConstructor(staticName = "of")
public class Money {

    private final int amount;
    private final Currency currency;

    public Money remove(Money money) {
        return Money.of(
                amount - toCurrency(money.getAmount(), money.getCurrency(), currency),
                currency);
    }

    public Money add(Money money) {
        return Money.of(
                amount + toCurrency(money.getAmount(), money.getCurrency(), currency),
                currency);
    }

    private int toCurrency(int amount, Currency from, Currency to) {
        if (from.equals(to)) {
            return amount;
        } else {
            throw new NotImplemented();
        }
    }

}
