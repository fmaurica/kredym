package fmg.kredym.model;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

import java.time.Instant;
import java.util.List;

@Value
@Builder
public class ToppingPrint {

    private final Id id;
    private final Money price;
    private final List<Topping> toppings;
    private String distributor;
    private final Instant creationInstant;

    @Value
    @RequiredArgsConstructor(staticName = "of")
    @ToString
    public static class Id extends fmg.kredym.model.Id {
        private final int value;
    }

}
