package fmg.kredym.model;

import fmg.kredym.exception.NotAuthorized;
import fmg.kredym.exception.NotImplemented;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

import java.util.Objects;

@Value
@Builder(toBuilder = true)
@ToString
public class Account {

    private final Id id;
    private final String name;
    private final Money money;
    private final boolean disabled;

    public void checkForMoneyRemoval(Money money) {
        if (!Objects.equals(this.money.getCurrency(), money.getCurrency())) {
            throw new NotImplemented();
        } else if (this.money.getAmount() - money.getAmount() < 0) {
            throw new NotAuthorized("Cannot remove money since account balance will be negative");
        }
    }

    public void checkForMoneyAdding(Money money) {
        // no restriction
    }

    @Value
    @RequiredArgsConstructor(staticName = "of")
    @ToString
    public static class Id extends fmg.kredym.model.Id {
        private final int value;
    }

}
