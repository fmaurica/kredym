package fmg.kredym.model.external;

import fmg.kredym.template.TemplatableHtml;
import lombok.Builder;
import lombok.SneakyThrows;
import lombok.Value;

import java.nio.file.FileSystems;
import java.util.List;

@Value
@Builder
public class ToppingPrintTemplatable extends TemplatableHtml {

    @Override
    protected void setTemplatePath() {
        templatePath = "/templates/toppingPrint.html";
    }

    @SneakyThrows
    @Override
    protected void setResourcesPath() {
        resourcesPath = FileSystems
                .getDefault()
                .getPath("src", "main", "resources", "templates")
                .toUri()
                .toURL()
                .toString();
    }

    private final String kredymBaseUrl;
    private final String id;

    private final String distributor;
    private final String creationDate;
    private final int sequence;
    private final int sequenceMax;
    private final int amountInMga;
    private final List<Topping> toppings;

    @Value
    @Builder
    public static class Topping {
        private final int amountInMga;
        private final String code;
        private final String expirationDate;
    }

}
