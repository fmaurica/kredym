package fmg.kredym.model.external;

import fmg.kredym.conf.Doc;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ToppingPrint {

    private final String id;
    private final Money price;
    private final List<Topping> toppings;
    private String distributor;

    @Schema(description = Doc.INSTANT, example = Doc.INSTANT_EXAMPLE)
    private final String creationInstant;

}
