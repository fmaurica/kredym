package fmg.kredym.model.external;

import fmg.kredym.conf.Doc;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Transaction {

    private final String transactionId;
    private final String sourceAccountId;
    private final String destinationAccountId;
    private final Money money;
    @Schema(description = Doc.INSTANT, example = Doc.INSTANT_EXAMPLE)
    private final String/*JSON has no specification for instants*/ creationInstant;

}
