package fmg.kredym.model.external;

import fmg.kredym.conf.Doc;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@Value
@ToString
@Builder
public class Topping {

    private String id;

    private final String code;
    private final Money price;

    @Schema(description = Doc.INSTANT, example = Doc.INSTANT_EXAMPLE)
    private final String creationInstant;
    @Schema(description = Doc.VALIDITY)
    private final long validity;

    @Schema(description = Doc.INSTANT, example = Doc.INSTANT_EXAMPLE)
    private final String activationInstant;
    @Schema(description = Doc.ACTIVATOR_ID)
    private final String activatorId;

    private final String printId;

}
