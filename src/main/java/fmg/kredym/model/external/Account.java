package fmg.kredym.model.external;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Account {

    private final String accountId;
    private final String name;
    private final Money money;

}
