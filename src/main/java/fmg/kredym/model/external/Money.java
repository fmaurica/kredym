package fmg.kredym.model.external;

import fmg.kredym.conf.Doc;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;

@Value(staticConstructor = "of")
public class Money {

    @Schema(description = Doc.AMOUNT)
    private final int amount;

    @Schema(description = Doc.CURRENCY, example = Doc.CURRENCY_EXAMPLE)
    private final String currency;

}
