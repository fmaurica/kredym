package fmg.kredym.model.external;

import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor(staticName = "of")
public class Error {

    private final Type errorType;
    private final String message;

    public enum Type {
        BAD_REQUEST, NOT_AUTHORIZED, NOT_FOUND, INTERNAL_ERROR, NOT_IMPLEMENTED
    }

}
