package fmg.kredym.model;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

import java.time.Instant;

@Value
@Builder
@ToString
public class Transaction {

    private final Id id;
    private final Account source;
    private final Account destination;
    private final Money money;
    private final Instant creationInstant;

    @Value
    @RequiredArgsConstructor(staticName = "of")
    @ToString
    public static class Id extends fmg.kredym.model.Id {
        private final int value;
    }

}
