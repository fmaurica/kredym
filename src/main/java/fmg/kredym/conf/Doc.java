package fmg.kredym.conf;

public class Doc {

    public static final String MONEY = "`amount,currency` such that " +
            "`amount` is in minor unit (e.g. Cent is the minor unit of Euro) " +
            "and `currency` is an ISO-4217 currency code. Hence `500,EUR` represents 500 Cents or 5€.";
    public static final String MONEY_EXAMPLE = "500,EUR";
    public static final String AMOUNT = "Amount in minor unit, e.g.: Cent is the minor unit of Euro";
    public static final String INSTANT = "ISO-8601 localized instant";
    public static final String INSTANT_EXAMPLE = "2011-12-03T10:15:30Z";
    public static final String CURRENCY = "ISO-4217 currency code";
    public static final String CURRENCY_EXAMPLE = "MGA";

    public static final int PAGINATION_MAXSIZE = 500;
    public static final String PAGINATION = "`page,size` such that `page>=0` " +
            "and `0<size<=" + PAGINATION_MAXSIZE + "`";
    public static final String PAGINATION_EXAMPLE = "2,50";

    public static final String VALIDITY = "Number of days after the creation instant during which the credit " +
            "can be activated";
    public static final String ACTIVATOR_ID = "Id of the account who activated the data credit";

}
