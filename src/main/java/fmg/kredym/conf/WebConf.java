package fmg.kredym.conf;

import fmg.kredym.controller.converter.*;
import fmg.kredym.interceptor.HttpLogger;
import fmg.kredym.security.IdCipher;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConf implements WebMvcConfigurer {

    private final IdCipher idCipher;

    public WebConf(IdCipher idCipher) {
        this.idCipher = idCipher;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HttpLogger());
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new AccountIdConverter(idCipher));
        registry.addConverter(new TransactionIdConverter(idCipher));
        registry.addConverter(new MoneyConverter());
        registry.addConverter(new InstantConverter());
        registry.addConverter(new PaginationConverter());
    }

}
