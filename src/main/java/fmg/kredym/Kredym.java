package fmg.kredym;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Kredym - The Malagasy API for Online Payments", version = "1.0"))
public class Kredym {

    public static void main(String[] args) {
        SpringApplication.run(Kredym.class, args);
    }

}
