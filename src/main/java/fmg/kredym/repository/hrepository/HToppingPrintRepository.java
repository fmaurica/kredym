package fmg.kredym.repository.hrepository;

import fmg.kredym.repository.hrepository.model.HToppingPrint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HToppingPrintRepository extends JpaRepository<HToppingPrint, Integer> {
}
