package fmg.kredym.repository.hrepository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "transaction")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HTransaction {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "sourceId")
    private HAccount source;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "destinationId")
    private HAccount destination;

    private int amount;
    private String currency;

    @CreationTimestamp
    private Instant creationInstant;

}
