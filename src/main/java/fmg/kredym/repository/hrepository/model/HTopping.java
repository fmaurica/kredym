package fmg.kredym.repository.hrepository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Entity(name = "Topping")
@Table(name = "topping")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HTopping {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;
    private String code;
    private int amount; // in major unit
    private String currency;
    private long validity; // in days

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "topping_print_id")
    private HToppingPrint toppingPrint;

    private Instant activationInstant;
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "activator_id")
    private HAccount activator;

    @CreationTimestamp
    private Instant creationInstant;

    private boolean disabled;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Mapping bi-directional one-to-many
    // https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/
    ///////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HTopping)) return false;
        return id != null && id.equals(((HTopping) o).getId());
    }

    @Override
    public int hashCode() {
        return 31; // Vlad said so...
    }

    @Override
    public String toString() {
        return "HTopping{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", validity=" + validity +
                ", toppingPrint(id)=" + toppingPrint.getId() + // infinite recursion otherwise
                ", activationInstant=" + activationInstant +
                ", activator=" + activator +
                ", creationInstant=" + creationInstant +
                ", disabled=" + disabled +
                '}';
    }

}
