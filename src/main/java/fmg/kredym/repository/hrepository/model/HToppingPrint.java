package fmg.kredym.repository.hrepository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Entity(name = "ToppingPrint")
@Table(name = "topping_print")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HToppingPrint {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;
    private int amount; // in major unit
    private String currency;
    private String distributor;

    @OneToMany(mappedBy = "toppingPrint", fetch = LAZY, cascade = ALL)
    private List<HTopping> toppings;

    @CreationTimestamp
    private Instant creationInstant;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Mapping bi-directional one-to-many
    // https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void addTopping(HTopping topping) {
        toppings.add(topping);
        topping.setToppingPrint(this);
    }

    public void removeTopping(HTopping topping) {
        toppings.remove(topping);
        topping.setToppingPrint(null);
    }

}
