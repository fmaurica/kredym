package fmg.kredym.repository.hrepository;

import fmg.kredym.repository.hrepository.model.HAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

import static javax.persistence.LockModeType.PESSIMISTIC_WRITE;

public interface HAccountRepository extends JpaRepository<HAccount, Integer> {

    @Lock(PESSIMISTIC_WRITE)
    @Query("select a from HAccount a where a.id = :id")
    Optional<HAccount> pwFindById(int id);

}
