package fmg.kredym.repository.hrepository;

import fmg.kredym.repository.hrepository.model.HTopping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface HToppingRepository extends JpaRepository<HTopping, Integer> {

    @Modifying
    @Query("UPDATE Topping t SET t.activator.id = :activatorId, t.activationInstant = CURRENT_TIMESTAMP " +
            "WHERE t.code = :code")
    void activate(String code, int activatorId);

    Optional<HTopping> findByCode(String code);

}
