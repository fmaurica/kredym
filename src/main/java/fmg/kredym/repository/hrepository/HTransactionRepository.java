package fmg.kredym.repository.hrepository;

import fmg.kredym.repository.hrepository.model.HTransaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HTransactionRepository extends JpaRepository<HTransaction, Integer> {

    Slice<HTransaction> findByAmountAndCurrency(int amount, String currency, Pageable pageable);

}
