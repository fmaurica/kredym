package fmg.kredym.repository;

import fmg.kredym.mapper.TransactionMapper;
import fmg.kredym.model.Money;
import fmg.kredym.model.Transaction;
import fmg.kredym.repository.hrepository.HTransactionRepository;
import fmg.kredym.repository.hrepository.model.HTransaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.validation.constraints.Null;
import java.time.Instant;
import java.util.*;

import static java.util.stream.Collectors.toUnmodifiableList;

@Repository
public class TransactionRepository {

    private final EntityManager em;
    private final HTransactionRepository hTransactionRepository;
    private final TransactionMapper transactionMapper;

    public TransactionRepository(EntityManager em,
                                 HTransactionRepository hTransactionRepository,
                                 TransactionMapper transactionMapper) {
        this.em = em;
        this.hTransactionRepository = hTransactionRepository;
        this.transactionMapper = transactionMapper;
    }

    public Transaction save(Transaction transaction) {
        HTransaction saved = hTransactionRepository.save(transactionMapper.mapToH(transaction));
        return transactionMapper.mapToDomain(saved);
    }

    public Optional<Transaction> findById(Transaction.Id id) {
        return hTransactionRepository
                .findById(id.getValue())
                .map(transactionMapper::mapToDomain);
    }

    public Slice/*_not_ Page since it has total (count) attribute*/<Transaction>
    findByMoney(Money money, Pageable pageable) {
        return hTransactionRepository
                .findByAmountAndCurrency(money.getAmount(), money.getCurrency().getCurrencyCode(), pageable)
                .map(transactionMapper::mapToDomain);
    }

    public List<Transaction> findByCriteria(@Null Transaction.Id id,
                                            @Nullable Money money,
                                            @Nullable Instant from,
                                            @Nullable Instant to,
                                            Pageable pageable) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        Map<ParameterExpression, Object> parameters = new HashMap<>();
        ParameterExpression<Integer> idParameter = cb.parameter(Integer.class);
        ParameterExpression<Integer> amountParameter = cb.parameter(Integer.class);
        ParameterExpression<String> currencyParameter = cb.parameter(String.class);
        ParameterExpression<Instant> fromParameter = cb.parameter(Instant.class);
        ParameterExpression<Instant> toParameter = cb.parameter(Instant.class);
        parameters.put(idParameter, id == null ? null : id.getValue());
        parameters.put(amountParameter, money == null ? null : money.getAmount());
        parameters.put(currencyParameter, money == null ? null : money.getCurrency().getCurrencyCode());
        parameters.put(fromParameter, from);
        parameters.put(toParameter, to);

        CriteriaQuery<HTransaction> cq = cb.createQuery(HTransaction.class);
        Root<HTransaction> root = cq.from(HTransaction.class);
        cq.select(root).where(buildNullableRestrictions(
                cb, root,
                parameters,
                idParameter,
                amountParameter,
                currencyParameter,
                fromParameter,
                toParameter));

        TypedQuery<HTransaction> tq = em.createQuery(cq);
        buildNullableParameters(tq, parameters);
        tq.setFirstResult((int) pageable.getOffset());
        tq.setMaxResults(pageable.getPageSize());
        String hQuery = tq.unwrap(org.hibernate.query.Query.class).getQueryString();

        return tq.getResultStream()
                .map(transactionMapper::mapToDomain)
                .collect(toUnmodifiableList());
    }

    private Predicate[] buildNullableRestrictions(
            CriteriaBuilder cb, Root<HTransaction> root,
            Map<ParameterExpression, Object> parameters,
            @Nullable ParameterExpression<Integer> idParameter,
            @Nullable ParameterExpression<Integer> amountParameter,
            @Nullable ParameterExpression<String> currencyParameter,
            @Nullable ParameterExpression<Instant> fromParameter,
            @Nullable ParameterExpression<Instant> toParameter) {
        List<Predicate> restrictions = new ArrayList<>();
        if (parameters.get(idParameter) != null) {
            restrictions.add(cb.equal(root.get("id"), idParameter));
        }
        if (parameters.get(amountParameter) != null && parameters.get(currencyParameter) != null) {
            restrictions.add(cb.equal(root.get("amount"), amountParameter));
            restrictions.add(cb.equal(root.get("currency"), currencyParameter));
        }
        if (parameters.get(fromParameter) != null) {
            restrictions.add(cb.greaterThanOrEqualTo(root.get("creationInstant"), fromParameter));
        }
        if (parameters.get(toParameter) != null) {
            restrictions.add(cb.lessThanOrEqualTo(root.get("creationInstant"), toParameter));
        }
        return restrictions.toArray(new Predicate[0]);
    }

    private void buildNullableParameters(TypedQuery<HTransaction> tq, Map<ParameterExpression, Object> parameters) {
        parameters.forEach((parameter, value) -> {
            if (value != null) {
                tq.setParameter(parameter, value);
            }
        });
    }

}
