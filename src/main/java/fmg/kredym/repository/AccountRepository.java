package fmg.kredym.repository;

import fmg.kredym.mapper.AccountMapper;
import fmg.kredym.model.Account;
import fmg.kredym.repository.hrepository.HAccountRepository;
import fmg.kredym.repository.hrepository.model.HAccount;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class AccountRepository {

    private final HAccountRepository hAccountRepository;
    private final AccountMapper accountMapper;

    public AccountRepository(HAccountRepository hAccountRepository, AccountMapper accountMapper) {
        this.hAccountRepository = hAccountRepository;
        this.accountMapper = accountMapper;
    }

    public Optional<Account> findById(Account.Id accountId) {
        return hAccountRepository
                .findById(accountId.getValue())
                .map(accountMapper::mapToDomain);
    }

    public Optional<Account> pwFindById(Account.Id accountId) {
        return hAccountRepository
                .pwFindById(accountId.getValue())
                .map(accountMapper::mapToDomain);
    }

    public Account save(Account account) {
        HAccount hAccount = accountMapper.mapToH(account);
        return accountMapper.mapToDomain(hAccountRepository.save(hAccount));
    }

}
