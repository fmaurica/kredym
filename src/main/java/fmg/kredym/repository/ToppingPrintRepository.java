package fmg.kredym.repository;

import fmg.kredym.exception.NotFound;
import fmg.kredym.mapper.ToppingPrintMapper;
import fmg.kredym.model.Account;
import fmg.kredym.model.Topping;
import fmg.kredym.model.ToppingPrint;
import fmg.kredym.repository.hrepository.HToppingPrintRepository;
import fmg.kredym.repository.hrepository.HToppingRepository;
import fmg.kredym.repository.hrepository.model.HToppingPrint;
import org.springframework.stereotype.Repository;

import static fmg.kredym.exception.NotFound.ResourceType.TOPPING;
import static fmg.kredym.model.Id.IMPOSSIBLE_VALUE;

@Repository
public class ToppingPrintRepository {

    private final HToppingPrintRepository hToppingPrintRepository;
    private final HToppingRepository hToppingRepository;
    private final ToppingPrintMapper toppingPrintMapper;

    public ToppingPrintRepository(HToppingPrintRepository hToppingPrintRepository,
                                  HToppingRepository hToppingRepository,
                                  ToppingPrintMapper toppingPrintMapper) {
        this.hToppingPrintRepository = hToppingPrintRepository;
        this.hToppingRepository = hToppingRepository;
        this.toppingPrintMapper = toppingPrintMapper;
    }

    public ToppingPrint save(ToppingPrint print) {
        HToppingPrint hToppingPrint = toppingPrintMapper.mapToH(print);
        HToppingPrint saved = hToppingPrintRepository.save(hToppingPrint);
        return toppingPrintMapper.mapToDomain(saved);
    }

    public Topping getToppingByCode(String code) {
        return toppingPrintMapper.mapToDomain(
                hToppingRepository
                        .findByCode(code)
                        .orElseThrow(() -> new NotFound(TOPPING, Topping.Id.of(IMPOSSIBLE_VALUE))));
    }

    public Topping activate(String code, Account.Id activatorId) {
        hToppingRepository.activate(code, activatorId.getValue());
        return getToppingByCode(code);
    }

}
