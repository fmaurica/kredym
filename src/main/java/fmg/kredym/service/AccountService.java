package fmg.kredym.service;

import fmg.kredym.exception.NotAuthorized;
import fmg.kredym.exception.NotFound;
import fmg.kredym.model.Account;
import fmg.kredym.model.Topping;
import fmg.kredym.repository.AccountRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static fmg.kredym.exception.NotFound.ResourceType.ACCOUNT;
import static org.springframework.transaction.annotation.Isolation.SERIALIZABLE;
import static org.springframework.transaction.annotation.Propagation.MANDATORY;

@Service
public class AccountService {

    private final AccountRepository accountRepository;
    private final ToppingPrintService toppingPrintService;

    public AccountService(AccountRepository accountRepository, ToppingPrintService toppingPrintService) {
        this.accountRepository = accountRepository;
        this.toppingPrintService = toppingPrintService;
    }

    public Optional<Account> findById(Account.Id accountId) {
        return accountRepository.findById(accountId);
    }

    @Transactional(propagation = MANDATORY)
    public Optional<Account> pwFindById(Account.Id accountId) {
        return accountRepository.pwFindById(accountId);
    }

    public Account save(Account account) {
        return accountRepository.save(account);
    }

    @Transactional(isolation = SERIALIZABLE)
    public Account topUp(Account.Id accountId, String code) {
        Account account = findById(accountId).orElseThrow(() -> new NotFound(ACCOUNT, accountId));
        if (account.isDisabled()) {
            throw new NotAuthorized("Account is disabled");
        }
        Topping activatedTopping = toppingPrintService.activate(code, account.getId());

        Account updatedAccount = account.toBuilder()
                .money(account.getMoney().add(activatedTopping.getPrice()))
                .build();
        return accountRepository.save(updatedAccount);
    }

}
