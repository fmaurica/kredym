package fmg.kredym.service;

import fmg.kredym.exception.NotFound;
import fmg.kredym.model.Account;
import fmg.kredym.model.Money;
import fmg.kredym.model.Transaction;
import fmg.kredym.repository.TransactionRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static fmg.kredym.exception.NotFound.ResourceType.ACCOUNT;
import static org.springframework.transaction.annotation.Isolation.SERIALIZABLE;

@Service
public class TransactionService {

    private final TransactionRepository repository;
    private final AccountService accountService;

    public TransactionService(TransactionRepository repository, AccountService accountService) {
        this.repository = repository;
        this.accountService = accountService;
    }

    public Optional<Transaction> findById(Transaction.Id id) {
        return repository.findById(id);
    }

    /* How is support for serializable isolation provided by postgres:
     * https://stackoverflow.com/questions/46545750/running-select-update-in-a-transaction-has-different-results-than-update-alone */
    @Transactional(isolation = SERIALIZABLE)
    public Transaction createTransaction(Account.Id sourceId, Account.Id destinationId, Money money) {
        Account source = findAccountById(sourceId);
        Account destination = findAccountById(destinationId);
        source.checkForMoneyRemoval(money);
        destination.checkForMoneyAdding(money);

        Account sourceUpdated = source.toBuilder()
                .money(source.getMoney().remove(money))
                .build();
        Account destinationUpdated = destination.toBuilder()
                .money(destination.getMoney().add(money))
                .build();
        Transaction transaction = Transaction.builder()
                .source(sourceUpdated)
                .destination(destinationUpdated)
                .money(money)
                .build();

        accountService.save(sourceUpdated);
        accountService.save(destinationUpdated);
        return repository.save(transaction);
    }

    @Transactional //TODO: stream+Transactional is a bad idea
    public Stream<Transaction> findByMoney(Money money, int page, int size) {
        return repository
                .findByMoney(money, PageRequest.of(page, size))
                .get();
    }

    @Transactional
    public List<Transaction> findByCriteria(@Nullable Transaction.Id id,
                                            @Nullable Money money,
                                            @Nullable Instant from,
                                            @Nullable Instant to,
                                            PageRequest pageRequest) {
        return repository.findByCriteria(id, money, from, to, pageRequest);
    }

    private Account findAccountById(Account.Id id) {
        return accountService
                .findById(id)
                .orElseThrow(() -> new NotFound(ACCOUNT, id));
    }

}
