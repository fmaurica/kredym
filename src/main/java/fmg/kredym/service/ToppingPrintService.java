package fmg.kredym.service;

import fmg.kredym.exception.NotAuthorized;
import fmg.kredym.exception.NotImplemented;
import fmg.kredym.model.Account;
import fmg.kredym.model.Money;
import fmg.kredym.model.Topping;
import fmg.kredym.model.ToppingPrint;
import fmg.kredym.repository.ToppingPrintRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ToppingPrintService {

    private final ToppingPrintRepository toppingPrintRepository;
    private final int codeLength;
    private final char[] codeChars = {
            // alphanumeric chars without '1' and 'l', which could be confused
            '0', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
            'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z'};

    public ToppingPrintService(ToppingPrintRepository toppingPrintRepository,
                               @Value("${kredym.topping.codeLength}") int codeLength) {
        this.toppingPrintRepository = toppingPrintRepository;
        this.codeLength = codeLength;
    }

    @Transactional
    public List<ToppingPrint> createToppingPrints(
            int printsNumber, Money printPrice, int toppingsPerPrint, String distributor,
            Money toppingPrice, Duration toppingValidity) {
        List<ToppingPrint> prints = new ArrayList<>();
        for (int i = 0; i < printsNumber; i++) {
            prints.add(toppingPrintRepository.save(
                    aToppingPrint(toppingsPerPrint, printPrice, distributor, toppingPrice, toppingValidity)));
        }
        return prints;
    }

    private ToppingPrint aToppingPrint(
            int toppingsPerPrint, Money printPrice, String distributor,
            Money toppingPrice, Duration toppingValidity) {
        List<Topping> toppings = new ArrayList<>();
        for (int i = 0; i < toppingsPerPrint; i++) {
            toppings.add(Topping.builder()
                    .code(aRandomCode()) //TODO: repository saving may fail due to uniqueness constraint
                    .price(toppingPrice)
                    .validity(toppingValidity)
                    .build());
        }
        return ToppingPrint.builder()
                .price(printPrice)
                .toppings(toppings)
                .distributor(distributor)
                .build();
    }

    private String aRandomCode() {
        StringBuilder codeBuilder = new StringBuilder();
        for (int i = 0; i < codeLength; i++) {
            codeBuilder.append(codeChars[(int) (codeChars.length * Math.random())]);
        }
        return codeBuilder.toString();
    }

    public Optional<Topping> findToppingById(Topping.Id id) {
        throw new NotImplemented();
    }

    public Topping activate(String code, Account.Id activatorId) {
        Topping topping = toppingPrintRepository.getToppingByCode(code);
        if (topping.getActivator() != null) {
            throw new NotAuthorized("Topping is already activated");
        } else if (topping.isDisabled()) {
            throw new NotAuthorized("Topping is disabled");
        }
        return toppingPrintRepository.activate(code, activatorId);
    }

}
