package fmg.kredym.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static javax.crypto.Cipher.DECRYPT_MODE;
import static javax.crypto.Cipher.ENCRYPT_MODE;

@Service
public class IdCipher {

    private final Cipher encrypter;
    private final Cipher decrypter;
    private static final String ALGORITHM = "Blowfish"; //"AES"
    private static final String TRANSFORMATION = "Blowfish"; //"AES/ECB/PKCS5Padding"

    public IdCipher(@Value("${kredym.id-cipher.key}") String key) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), ALGORITHM);
            encrypter = Cipher.getInstance(TRANSFORMATION);
            decrypter = Cipher.getInstance(TRANSFORMATION);
            encrypter.init(ENCRYPT_MODE, secretKeySpec);
            decrypter.init(DECRYPT_MODE, secretKeySpec);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public String encrypt(String s) {
        try {
            return Base64.getUrlEncoder().encodeToString(encrypter.doFinal(s.getBytes()));
        } catch (Exception e) {
            throw new CipherException(e);
        }
    }

    public String decrypt(String s) {
        try {
            return new String(decrypter.doFinal(Base64.getUrlDecoder().decode(s.getBytes())));
        } catch (Exception e) {
            throw new CipherException(e);
        }
    }

}
