package fmg.kredym.security;

public class CipherException extends RuntimeException {

    public CipherException(Throwable cause) {
        super(cause);
    }

}
