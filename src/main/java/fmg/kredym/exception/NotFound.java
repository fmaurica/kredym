package fmg.kredym.exception;

import fmg.kredym.model.Id;
import lombok.Getter;

public class NotFound extends RuntimeException {

    @Getter
    private final ResourceType resourceType;
    @Getter
    private final Id id;

    private static final String MESSAGE_FORMAT = "resourceType=%s, id=%s";

    public enum ResourceType {
        ACCOUNT, TRANSACTION, TOPPING
    }

    public NotFound(ResourceType resourceType, Id id) {
        super(getErrorMessage(resourceType, id.getValue() + ""));
        this.resourceType = resourceType;
        this.id = id;
    }

    public static String getErrorMessage(ResourceType resourceType, String id) {
        return String.format(MESSAGE_FORMAT, resourceType, id);
    }

}
