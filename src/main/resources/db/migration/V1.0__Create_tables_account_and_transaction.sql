create table if not exists account (
    id       serial      constraint account_pk primary key,
    name     varchar(50) not null,
    amount   integer     not null,
    currency varchar(10) not null
);

create table if not exists TRANSACTION (
    id               serial      constraint transaction_pk primary key,
    source_id        integer     references account on update restrict on delete restrict,
    destination_id   integer     references account on update restrict on delete restrict,
    amount           integer     not null,
    currency         varchar(10) not null,
    creation_instant timestamp
                     with
                     time zone   not null DEFAULT current_timestamp
);
