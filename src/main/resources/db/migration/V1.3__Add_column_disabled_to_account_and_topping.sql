alter table account add column if not exists disabled boolean default false;
alter table topping add column if not exists disabled boolean default false;
