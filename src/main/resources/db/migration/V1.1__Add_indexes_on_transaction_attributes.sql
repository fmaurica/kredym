create index concurrently if not exists transaction_creation_instant_idx on transaction (creation_instant);
create index concurrently if not exists transaction_money_idx on transaction (amount, currency);
