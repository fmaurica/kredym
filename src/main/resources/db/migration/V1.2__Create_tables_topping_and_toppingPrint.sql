create table if not exists topping_print
(
    id               serial        constraint topping_print_pk primary key,
    amount           integer       not null,
    currency         varchar(10)   not null,
    distributor      varchar(50)   not null,
    creation_instant timestamp
                     with
                     time zone     not null default current_timestamp
);

create table if not exists topping (
    id                 serial      constraint topping_pk primary key,
    code               varchar(20) not null,
    amount             integer     not null,
    currency           varchar(10) not null,
    validity           integer     not null,
    topping_print_id   integer     references topping_print on update restrict on delete restrict,
    activation_instant timestamp
                       with
                       time zone,
    activator_id       integer     references account on update restrict on delete restrict,
    creation_instant   timestamp
                       with
                       time zone   not null default current_timestamp
);
