package fmg.kredym.service;

import fmg.kredym.exception.NotAuthorized;
import fmg.kredym.model.Account;
import fmg.kredym.model.Money;
import fmg.kredym.model.Transaction;
import fmg.kredym.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.Currency;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class TransactionServiceTest {

    private TransactionService subject;
    private TransactionRepository transactionRepository;
    private AccountService accountService;
    private static final Currency EUR = Currency.getInstance("EUR");

    @BeforeEach
    public void setUp() {
        accountService = mock(AccountService.class);
        transactionRepository = mock(TransactionRepository.class);
        subject = new TransactionService(transactionRepository, accountService);
    }

    @Test
    public void no_removal_if_insufficient_fund() {
        // given
        Account account1 = anAccount(1, 10);
        Account account2 = anAccount(2, 20);
        when(accountService.findById(Account.Id.of(1))).thenReturn(Optional.of(account1));
        when(accountService.findById(Account.Id.of(2))).thenReturn(Optional.of(account2));

        // when
        assertThrows(
                NotAuthorized.class,
                () -> subject.createTransaction(account1.getId(), account2.getId(), aMoney(30)));
    }

    @Test
    public void createTransaction_if_sufficient_fund() {
        // given
        Account account1 = anAccount(1, 10);
        Account account2 = anAccount(2, 20);
        when(accountService.findById(Account.Id.of(1))).thenReturn(Optional.of(account1));
        when(accountService.findById(Account.Id.of(2))).thenReturn(Optional.of(account2));

        // when
        subject.createTransaction(account1.getId(), account2.getId(), aMoney(5));

        // then
        ArgumentCaptor<Transaction> captor = ArgumentCaptor.forClass(Transaction.class);
        verify(transactionRepository, times(1)).save(captor.capture());
        Transaction transaction = captor.getValue();
        assertEquals(5, transaction.getMoney().getAmount());
        assertEquals(EUR, transaction.getMoney().getCurrency());
        assertEquals(5, transaction.getSource().getMoney().getAmount());
        assertEquals(25, transaction.getDestination().getMoney().getAmount());
    }

    private Account anAccount(int id, int amount) {
        return Account.builder()
                .id(Account.Id.of(id))
                .money(aMoney(amount))
                .build();
    }

    private Money aMoney(int amount) {
        return Money.of(amount, EUR);
    }

}
