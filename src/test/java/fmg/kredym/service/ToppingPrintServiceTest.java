package fmg.kredym.service;

import fmg.kredym.model.Money;
import fmg.kredym.model.Topping;
import fmg.kredym.model.ToppingPrint;
import fmg.kredym.repository.ToppingPrintRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Currency;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

public class ToppingPrintServiceTest {

    private ToppingPrintService subject;
    private ToppingPrintRepository toppingPrintRepository;
    private final Currency MGA = Currency.getInstance("MGA");
    private final int codeLength = 9;

    @BeforeEach
    public void setUp() {
        toppingPrintRepository = mock(ToppingPrintRepository.class);
        when(toppingPrintRepository.save(any())).then(returnsFirstArg());

        subject = new ToppingPrintService(toppingPrintRepository, codeLength);
    }

    @Test
    public void all_code_are_different_and_are_of_appropriate_length() {
        // given
        int printNumber = 10;
        Money printPrice = Money.of(100, MGA);
        int toppingPerPrint = 32;
        String distributor = "dummy";
        Money toppingPrice = Money.of(10, MGA);
        Duration validity = Duration.ofDays(14);

        // when
        List<ToppingPrint> prints = subject.createToppingPrints(
                printNumber, printPrice, toppingPerPrint, distributor,
                toppingPrice, validity);

        // then
        assertEquals(printNumber, prints.size());
        long distinctCodesNumber = prints.stream()
                .flatMap(print -> print.getToppings().stream())
                .map(Topping::getCode)
                .distinct()
                .peek(this::checkCodeLength)
                .count();
        assertEquals(printNumber * toppingPerPrint, distinctCodesNumber);
        verify(toppingPrintRepository, times(printNumber)).save(any());
    }

    private void checkCodeLength(String code) {
        if (codeLength != code.length()) {
            throw new RuntimeException("Bad code length: code=" + code + ", expectedLength=" + codeLength);
        }
    }

}
