package fmg.kredym.template;

import fmg.kredym.model.external.ToppingPrintTemplatable;
import fmg.kredym.model.external.ToppingPrintTemplatable.Topping;
import fmg.kredym.util.FileUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class TemplateEngineTest {

    private TemplateEngine subject;

    @BeforeEach
    public void setUp() {
        subject = new ThTemplateEngine();
    }

    @SneakyThrows
    @Test
    public void print_with_two_toppings() {
        // given
        ToppingPrintTemplatable print = ToppingPrintTemplatable.builder()
                .kredymBaseUrl("http://kredym.com")
                .id("anId")
                .distributor("Some school")
                .creationDate("22/11/1999")
                .sequence(7)
                .sequenceMax(10)
                .amountInMga(1800)
                .toppings(List.of(
                        Topping.builder().amountInMga(1000).code("code1").expirationDate("30/11/1999").build(),
                        Topping.builder().amountInMga(1000).code("code2").expirationDate("30/11/1999").build()))
                .build();

        // when
        String html = subject.process(print);
        File htmlTemp = FileUtil.createTempFile("print_with_two_toppings", "html", html);
        log.debug("Generated html: {}", htmlTemp.getAbsolutePath());
        File pdfTemp = subject.processToPdf(print, "print");
        log.debug("Generated pdf: {}", pdfTemp.getAbsolutePath());

        // then
        assertEquals(FileUtil.readResourceFile("/templates/print_with_two_toppings.html"), html);
    }

}
