package fmg.kredym.mapper;

import fmg.kredym.model.external.Money;
import fmg.kredym.model.external.ToppingPrint;
import fmg.kredym.model.external.ToppingPrintTemplatable;
import fmg.kredym.security.IdCipher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class ToppingPrintMapperTest {

    private ToppingPrintMapper subject;

    @BeforeEach
    public void setUp() {
        subject = new ToppingPrintMapper(mock(IdCipher.class), mock(AccountMapper.class));
    }

    @Test
    public void mapToTemplate() {
        // given
        ToppingPrint print = ToppingPrint.builder()
                .id("ciphered")
                .distributor("US School")
                .price(Money.of(700, "MGA"))
                .creationInstant("2010-01-01T08:00:00Z")
                .toppings(List.of(fmg.kredym.model.external.Topping.builder()
                        .code("1234")
                        .price(Money.of(5, "MGA"))
                        .creationInstant("2010-01-01T08:00:00Z")
                        .validity(21)
                        .build()))
                .build();

        // when
        ToppingPrintTemplatable printTemplatable = subject.mapToTemplatable(print, 7, 10);

        // then
        assertEquals("ciphered", printTemplatable.getId());
        assertEquals("US School", printTemplatable.getDistributor());
        assertEquals(700, printTemplatable.getAmountInMga());
        assertEquals("01/01/2010", printTemplatable.getCreationDate());
        List<ToppingPrintTemplatable.Topping> toppingsTemplatable = printTemplatable.getToppings();
        assertEquals(1, toppingsTemplatable.size());
        ToppingPrintTemplatable.Topping toppingTemplatable = toppingsTemplatable.get(0);
        assertEquals("1234", toppingTemplatable.getCode());
        assertEquals(5, toppingTemplatable.getAmountInMga());
        assertEquals("22/01/2010", toppingTemplatable.getExpirationDate());
    }

}
