package fmg.kredym.security;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class IdCipherTest {

    private IdCipher cipher;

    @BeforeEach
    public void setUp() {
        cipher = new IdCipher("secret");
    }

    @Test
    public void encryptAndDecrypt() {
        // given
        String id = "3";

        // when
        // (1,secret):uiAA887LF9E= (2,secret):dLUIqlIJYbc= (3,secret): jyvZHWWecMM=
        String encrypted = cipher.encrypt(id);
        String decrypted = cipher.decrypt(encrypted);

        // then
        assertNotEquals(id, encrypted);
        assertNotEquals(encrypted, decrypted);
        assertEquals(id, decrypted);
    }

}
