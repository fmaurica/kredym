package fmg.kredym.integration;

import fmg.kredym.TrackTime;
import fmg.kredym.controller.AccountController;
import fmg.kredym.controller.TransactionController;
import fmg.kredym.controller.request.CreateTransactionRequest;
import fmg.kredym.model.Account;
import fmg.kredym.security.IdCipher;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static java.lang.System.currentTimeMillis;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(TrackTime.class)
@Slf4j
public class ConcurrentTransactionsIT /*IT postfix is so that failsafe detects it*/ {

    @Autowired
    private AccountController accountController;
    @Autowired
    private TransactionController transactionController;
    @MockBean
    private IdCipher idCipher;

    @BeforeEach
    public void setUp() {
        when(idCipher.encrypt(anyString())).then(returnsFirstArg());
        when(idCipher.decrypt(anyString())).then(returnsFirstArg());
    }

    @Test
    public void concurrently_remove_one_at_a_time() {
        // given
        Account.Id sourceId = Account.Id.of(1);
        Account.Id destinationId = Account.Id.of(2);
        int initialAmount = accountController.getAccount(sourceId).get(0).getMoney().getAmount();
        int toRemove = 100; //10_000;

        // when
        CountDownLatch latch = new CountDownLatch(1);
        List<Future<Boolean>> futureRemovals = concurrentlyRemoveOneAtATime(latch, sourceId, destinationId, toRemove);
        latch.countDown();

        // then
        long succeeded = futureRemovals.stream()
                .filter(this::getFutureResult)
                .count();
        // if Serializable: toRemove=10k => succeeded=2k, executionTime=15s
        // executionTime for Serializable is mostly due to I/O, however errors must be logged => I/O unavoidable
        log.info("concurrently_remove_one_at_a_time: succeeded/toRemove={}/{}", succeeded, toRemove);
        assertTrue(succeeded > 0);
        assertTrue(succeeded <= toRemove);
        assertEquals(
                initialAmount - succeeded,
                accountController.getAccount(sourceId).get(0).getMoney().getAmount());
    }

    @Test
    public void no_concurrent_slow_read_during_concurrent_remove() {
        // given
        Account.Id sourceId = Account.Id.of(1);
        Account.Id destinationId = Account.Id.of(2);
        int toRemove = 100; //10_000;
        int nbReads = 10; //1_000;
        Duration slowReadThreshold = Duration.of(3, SECONDS);

        // when
        CountDownLatch latch = new CountDownLatch(1);
        concurrentlyRemoveOneAtATime(latch, sourceId, destinationId, toRemove);
        List<Future<Duration>> futureSourceReads = concurrentlyRead(latch, sourceId, nbReads);
        List<Future<Duration>> futureDestinationReads = concurrentlyRead(latch, destinationId, nbReads);
        latch.countDown();

        // then
        long slowSourceReads = countSlowReads(futureSourceReads, slowReadThreshold);
        long slowDestinationReads = countSlowReads(futureDestinationReads, slowReadThreshold);
        assertEquals(0, slowSourceReads);
        assertEquals(0, slowDestinationReads);
    }

    private List<Future<Boolean>> concurrentlyRemoveOneAtATime(
            CountDownLatch latch, Account.Id sourceId, Account.Id destinationId, int times) {
        int initialAmount = accountController.getAccount(sourceId).get(0).getMoney().getAmount();
        assertTrue(initialAmount > times);
        ExecutorService executorService = newFixedThreadPool(times);
        List<Future<Boolean>> futures = new ArrayList<>();
        for (int t = 0; t < times; t++) {
            futures.add(executorService.submit(() -> removeOne(sourceId, destinationId, latch)));
        }
        return futures;
    }

    @SneakyThrows
    private boolean removeOne(Account.Id sourceId, Account.Id destinationId, CountDownLatch latch) {
        latch.await();
        try {
            transactionController.createTransaction(CreateTransactionRequest.builder()
                    .sourceAccountId(sourceId.getValue() + "")
                    .destinationAccountId(destinationId.getValue() + "")
                    .amount(1)
                    .currency("EUR")
                    .build());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @SneakyThrows
    private List<Future<Duration>> concurrentlyRead(CountDownLatch latch, Account.Id accountId, int times) {
        ExecutorService executorService = newFixedThreadPool(times);
        List<Future<Duration>> futures = new ArrayList<>();
        for (int t = 0; t < times; t++) {
            futures.add(executorService.submit(() -> concurrentlyRead(latch, accountId)));
        }
        return futures;
    }

    @SneakyThrows
    private Duration concurrentlyRead(CountDownLatch latch, Account.Id accountId) {
        latch.await();
        long startTime = currentTimeMillis();
        accountController.getAccount(accountId);
        return Duration.of(currentTimeMillis() - startTime, MILLIS);
    }

    private long countSlowReads(List<Future<Duration>> futureSourceReads, Duration threshold) {
        return futureSourceReads.stream()
                .map(this::getFutureResult)
                .filter(duration -> duration.compareTo(threshold) > 0)
                .count();
    }

    @SneakyThrows
    private <T> T getFutureResult(Future<T> future) {
        return future.get();
    }

}
