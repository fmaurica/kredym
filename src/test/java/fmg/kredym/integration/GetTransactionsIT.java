package fmg.kredym.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class GetTransactionsIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void noCriteria_thenBadRequest() throws Exception {
        mockMvc.perform(get("/transactions"))
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"errorType\":\"BAD_REQUEST\",\"message\":\"One criteria must be provided at least\"}"));
    }

    @Test
    public void whenNoId_thenRequirePagination() throws Exception {
        String expectedContent = "{\"errorType\":\"BAD_REQUEST\",\"message\":\"Pagination must be provided when id is not\"}";
        mockMvc.perform(get("/transactions?money=20,EUR"))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedContent));
        mockMvc.perform(get("/transactions?from=2010-01-01T08:00:00Z"))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedContent));
        mockMvc.perform(get("/transactions?to=2010-01-01T08:00:00Z"))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedContent));
    }

    @Test
    public void byId_doesNotRequirePagination() throws Exception {
        mockMvc.perform(get("/transactions?id=dLUIqlIJYbc="))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"transactionId\":\"dLUIqlIJYbc=\",\"sourceAccountId\":\"uiAA887LF9E=\",\"destinationAccountId\":\"dLUIqlIJYbc=\",\"money\":{\"amount\":20,\"currency\":\"EUR\"},\"creationInstant\":\"2010-01-01T08:00:00Z\"}]"));
    }

    @Test
    public void byId_internalNotFound_thenExternalEmptyList() throws Exception {
        mockMvc.perform(get("/transactions?id=IMPOSSIBLE"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    public void byMoney() throws Exception {
        mockMvc.perform(get("/transactions?money=20,EUR&pagination=0,5"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"transactionId\":\"dLUIqlIJYbc=\",\"sourceAccountId\":\"uiAA887LF9E=\",\"destinationAccountId\":\"dLUIqlIJYbc=\",\"money\":{\"amount\":20,\"currency\":\"EUR\"},\"creationInstant\":\"2010-01-01T08:00:00Z\"}]"));
    }

    @Test
    public void byMoney_badPagination() throws Exception {
        String message = "{\"errorType\":\"BAD_REQUEST\",\"message\":\"Pagination format must be `page,size` such that `page>=0` and `0<size<=500`\"}";
        mockMvc.perform(get("/transactions?money=20,EUR&pagination=-1,5"))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(message));
        mockMvc.perform(get("/transactions?money=20,EUR&pagination=0,501"))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(message));
    }

    @Test
    public void byMoney_andByInstant() throws Exception {
        mockMvc.perform(get("/transactions?money=20,EUR&from=2010-01-01T08:00:00Z&to=2010-01-01T08:00:00Z&pagination=0,5"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"transactionId\":\"dLUIqlIJYbc=\",\"sourceAccountId\":\"uiAA887LF9E=\",\"destinationAccountId\":\"dLUIqlIJYbc=\",\"money\":{\"amount\":20,\"currency\":\"EUR\"},\"creationInstant\":\"2010-01-01T08:00:00Z\"}]"));
    }

}
