package fmg.kredym.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import fmg.kredym.controller.AccountController;
import fmg.kredym.controller.request.TopUpAccountRequest;
import fmg.kredym.model.Account;
import fmg.kredym.repository.hrepository.HToppingRepository;
import fmg.kredym.repository.hrepository.model.HTopping;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static junit.framework.TestCase.assertTrue;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class TopUpAccountIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AccountController accountController;
    @Autowired
    HToppingRepository hToppingRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void accountDisabled() throws Exception {
        TopUpAccountRequest req = TopUpAccountRequest.of("jyvZHWWecMM=", "w605zvpou");
        mockMvc
                .perform(post("/account/topUp")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(req)))
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andExpect(content().json("{\"errorType\":\"NOT_AUTHORIZED\",\"message\":\"Account is disabled\"}"));
    }

    @Test
    public void accountNotFound() throws Exception {
        TopUpAccountRequest req = TopUpAccountRequest.of("Impossible account", "Enabled code");
        mockMvc
                .perform(post("/account/topUp")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(req)))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(content().json("{\"errorType\":\"NOT_FOUND\",\"message\":\"resourceType=ACCOUNT, id=<id you provided>\"}"));
    }

    @Test
    public void codeDisabled() throws Exception {
        TopUpAccountRequest req = TopUpAccountRequest.of("uiAA887LF9E=", "Disabled code");
        mockMvc
                .perform(post("/account/topUp")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(req)))
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andExpect(content().json("{\"errorType\":\"NOT_AUTHORIZED\",\"message\":\"Topping is disabled\"}"));
    }

    @Test
    public void codeNotFound() throws Exception {
        TopUpAccountRequest req = TopUpAccountRequest.of("uiAA887LF9E=", "Impossible code");
        mockMvc
                .perform(post("/account/topUp")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(req)))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(content().json("{\"errorType\":\"NOT_FOUND\",\"message\":\"resourceType=TOPPING, id=<id you provided>\"}"));
    }

    @Test
    @DirtiesContext
    public void topUp() throws Exception {
        // given
        String code = "Enabled code";
        enableTopping(code);
        fmg.kredym.model.external.Account account = accountController.getAccount(Account.Id.of(1)).get(0);

        // when
        TopUpAccountRequest req = TopUpAccountRequest.of("uiAA887LF9E=", code);
        mockMvc
                .perform(post("/account/topUp")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(req)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        fmg.kredym.model.external.Account accountUpdated = accountController.getAccount(Account.Id.of(1)).get(0);
        assertTrue(accountUpdated.getMoney().getAmount() > account.getMoney().getAmount());

        HTopping hTopping = hToppingRepository.findByCode(code).orElseThrow();
        assertEquals(1, (int) hTopping.getActivator().getId());
        assertNotNull(hTopping.getActivationInstant());
    }

    private void enableTopping(String code) {
        HTopping hTopping = hToppingRepository.findByCode(code).orElseThrow();
        hTopping.setDisabled(false);
        hTopping.setActivator(null);
        hTopping.setActivationInstant(null);
        hToppingRepository.save(hTopping);
    }

}
