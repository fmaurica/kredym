package fmg.kredym.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fmg.kredym.controller.PrintControllerFacade;
import fmg.kredym.controller.request.CreateToppingPrintsRequest;
import fmg.kredym.model.external.Money;
import fmg.kredym.model.external.Topping;
import fmg.kredym.model.external.ToppingPrint;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static fmg.kredym.controller.PrintControllerFacade.PRINT_MGA_AMOUNT;
import static fmg.kredym.controller.PrintControllerFacade.TOPPING_MGA_AMOUNT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class PostToppingPrintIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void create() throws Exception {
        // given
        int printsNumber = 2;
        CreateToppingPrintsRequest req = CreateToppingPrintsRequest.builder()
                .distributor("Lambda distributor")
                .printsNumber(2)
                .build();

        // when
        MvcResult mvcResult = mockMvc
                .perform(post("/topping/prints")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(req)))
                .andReturn();

        // then
        MockHttpServletResponse httpResponse = mvcResult.getResponse();
        assertEquals(HttpStatus.CREATED.value(), httpResponse.getStatus());

        List<ToppingPrint> prints = objectMapper.readValue(
                //@formatter:off
                httpResponse.getContentAsString(), new TypeReference<>() {});
                //@formatter:on
        assertEquals(printsNumber, prints.size());
        long createdToppingsNumber = prints.stream()
                .peek(this::checkCreatedPrint)
                .flatMap(print -> print.getToppings().stream())
                .peek(this::checkCreatedTopping)
                .count();
        assertEquals(printsNumber * PrintControllerFacade.TOPPINGS_PER_PRINT, createdToppingsNumber);
    }

    private void checkCreatedPrint(ToppingPrint print) {
        List<String> errors = new ArrayList<>();

        Money price = print.getPrice();
        if (price == null || (PRINT_MGA_AMOUNT != price.getAmount() && !"MGA".equals(price.getCurrency()))) {
            errors.add("print price must be MGA " + PRINT_MGA_AMOUNT);
        }
        print.getToppings().forEach(topping -> {
            String printId = print.getId();
            String toppingPrintId = topping.getPrintId();
            if (toppingPrintId == null || !toppingPrintId.equals(printId)) {
                errors.add("printId=" + printId + " must be NOT null and equals topping.printId=" + toppingPrintId);
            }
        });

        if (errors.size() > 0) {
            throw new RuntimeException("Created print has errors: topping=" + print.getId() + ", errors=" + errors);
        }
    }

    private void checkCreatedTopping(Topping topping) {
        List<String> errors = new ArrayList<>();
        if (topping.getId() == null) {
            errors.add("id must NOT be null");
        }
        if (topping.getCode() == null) {
            errors.add("code must NOT be null");
        }
        Money price = topping.getPrice();
        if (TOPPING_MGA_AMOUNT != price.getAmount() && !"MGA".equals(price.getCurrency())) {
            errors.add("topping price must be MGA " + TOPPING_MGA_AMOUNT);
        }
        if (topping.getValidity() <= 0) {
            errors.add("validity must be >0");
        }
        if (topping.getPrintId() == null) {
            errors.add("printId must NOT be null");
        }
        if (topping.getActivationInstant() != null) {
            errors.add("activationInstant must be null");
        }
        if (topping.getActivatorId() != null) {
            errors.add("activator must be null");
        }
        if (topping.getCreationInstant() == null) {
            errors.add("creationInstant must NOT be null");
        }
        if (errors.size() > 0) {
            throw new RuntimeException("Created topping has errors: topping=" + topping + ", errors=" + errors);
        }
    }

}
