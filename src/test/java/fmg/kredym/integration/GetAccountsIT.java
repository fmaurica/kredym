package fmg.kredym.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false/*for disabling security*/)
public class GetAccountsIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void byId() throws Exception {
        mockMvc.perform(get("/accounts?id=uiAA887LF9E="))
                //TODO? use style of PostTransactionIT::create
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("uiAA887LF9E=")))
                .andExpect(content().string(containsString("EUR")))
                .andExpect(content().string(containsString("Lou BPFV")));
    }

    @Test
    public void byId_internalNotFound_thenExternalEmptyList() throws Exception {
        mockMvc.perform(get("/accounts?id=IMPOSSIBLE"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

}
