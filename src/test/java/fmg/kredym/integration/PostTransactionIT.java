package fmg.kredym.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import fmg.kredym.controller.request.CreateTransactionRequest;
import fmg.kredym.model.external.Money;
import fmg.kredym.model.external.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class PostTransactionIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void create() throws Exception {
        // given
        CreateTransactionRequest req = CreateTransactionRequest.builder()
                .sourceAccountId("uiAA887LF9E=")
                .destinationAccountId("dLUIqlIJYbc=")
                .amount(8)
                .currency("EUR")
                .build();

        // when
        MvcResult mvcResult = mockMvc
                .perform(post("/transaction")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(req)))
                .andReturn();

        // then
        MockHttpServletResponse httpResponse = mvcResult.getResponse();
        assertEquals(HttpStatus.CREATED.value(), httpResponse.getStatus());

        Transaction transaction = objectMapper.readValue(httpResponse.getContentAsString(), Transaction.class);
        assertEquals("uiAA887LF9E=", transaction.getSourceAccountId());
        assertEquals("dLUIqlIJYbc=", transaction.getDestinationAccountId());
        Money money = transaction.getMoney();
        assertEquals(8, money.getAmount());
        assertEquals("EUR", money.getCurrency());
        assertNotNull(transaction.getCreationInstant());
    }

    @Test
    public void create_internalNotFound_thenExternalNotFound() throws Exception {
        // given
        CreateTransactionRequest req = CreateTransactionRequest.builder()
                .sourceAccountId("uiAA887LF9E=")
                .destinationAccountId("IMPOSSIBLE")
                .amount(8)
                .currency("EUR")
                .build();

        // when-then
        mockMvc
                .perform(post("/transaction")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isNotFound())
                //TODO: <id you provided> --> IMPOSSIBLE
                .andExpect(content().json("{\"errorType\":\"NOT_FOUND\",\"message\":\"resourceType=ACCOUNT, id=<id you provided>\"}"));
    }

}
