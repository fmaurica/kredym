insert into account (id, name, amount, currency) values (1, 'Lou BPFV', 1000000000, 'EUR');
insert into account (id, name, amount, currency) values (2, 'Ryan Boursorama', 200, 'EUR');

insert into transaction (id, source_id, destination_id, amount, currency, creation_instant) values (1, 1, 2, 10, 'EUR', '2010-01-01 11:00:00+03');
insert into transaction (id, source_id, destination_id, amount, currency, creation_instant) values (2, 1, 2, 20, 'EUR', '2010-01-01 11:00:00+03');